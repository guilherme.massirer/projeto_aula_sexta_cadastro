// GRUPO 3
// Projeto de Solicitação de Cartão
// O usuário que irá utilizar o Sistema será o funcionário do Banco. 

//O Programa inicia-se com a inclusão das bibliotecas, definidas com #. 
#include <stdio.h> //leitura de dados, salvando informações de entrada e saída. 
#include <stdlib.h> //Necessário para utilizar o comando system ("cls").
#include <string.h> //contem funções para manipular strings.
#include <locale.h> //É um localizador para que o programa seja adaptado aos idiomas ou regiões determinados.
#include <conio.h> //A conio trás funções úteis para manipular caracteres na tela, especificar cor de caracter e de fundo.

struct nodo{  
             struct nodo *prox; // *prox é um ponteiro
             char nome[30], nomead[30], sexo[2], sexoad[2], cpfad[12], cpf[12], fone[10], fonead[10], rua[15], num[6];
             char comp[10], estado[25], cid[25], band[20];
             int  idade,idadead, cod, rend, esc, rg;
             } *inicio=NULL;    // * indica que é um ponteiro   // inicial é a variável
                                // Quando se cria um ponteiro, não precisa definir quantidade de espaços em memória. 
                                
 void cabecalho();

/*----------------------------------------------------------------------------------------------------------------------*/                             
int menu() //int - valor de retorno e o que fica dentro de parenteses é o que se recebe.
{
        int escolha;
        cabecalho();
        printf("\t     ---------------------------------------------    \n");
        printf("\t                 --- MENU INICIAL ---        \n");
        printf("\t     =============================================    \n");
        printf("\t     |[ 1 ] Preencher cadastro de solicitação    |      \n");
        printf("\t     |[ 2 ] Listar cadastro existente no Sistema |      \n");
        printf("\t     |[ 3 ] Editar cadastro existente no Sistema |      \n");//
        printf("\t     |[ 4 ] Pesquisar código do Cliente          |      \n");//
        printf("\t     |[ 5 ] Exluir cadastro                      |      \n");
        printf("\t     |[ 6 ] Sair                                 |      \n");
        printf("\t     ---------------------------------------------  \n\n\n");
        printf("-> ");
        scanf("%d",&escolha);
        fflush(stdin); // Limpa buffer de teclado
        system("cls");
        return (escolha);
}

/*----------------------------------------------------------------------------------------------------------------------*/

struct nodo *cadastrar_primeiro(int cod) 

/* Entre parenteses declaramos "cod" como inteiro, então não é necessário declarar
 "cod" abaixo como as outras variáveis*/
// Esta struct recebe o primeiro cadastro 
{
 	 //Cria ponteiro
	 FILE *arq;
	 //Cria o arquivo onde o exec está armazenado no computador
	 arq = fopen("info.txt","w");  
 
       int  idade, idadead, rend, esc, rg; //Declaração de variáveis do tipo inteiro
       char sexo[2], sexoad[2], nome[30], nomead[30], cpfad[12], cpf[12], band[20];//Declaração de variáveis do tipo char
     
    struct nodo *primeiro;
    primeiro = (struct nodo *) malloc (sizeof(struct nodo)); // Reservando um espaço em memória para receber primeiro.
    
    cabecalho();
    fflush (stdin); //Serve para limpaza de buffer
    printf("\tCliente possui restrição de crédito? \n"); //"printf" exibe mensagem em tela.
    printf("\tDigite 1 para sim e 2 para não: ");
    //CASO O CLIENTE TENHA RESTRIÇÃO DE CRÉDITO, NÃO SERÁ POSSIVEL REALIZAR O CADASTRO.
    scanf("%d", &esc);
   
    if (esc == 2){ //CASO O CLIENTE NÃO TENHA RESTRIÇÃO DE CRÉDITO, IRÁ SER INICIADO O CADASTRO
        system("cls");
        cabecalho();
        fflush(stdin);
	    printf("\n\t--------- Informações para o Cartão ----------\n\n");
	    fflush (stdin); //Serve para limpaza de buffer.
	    printf("\tO cliente deseja cartão do tipo Crédito ou Débito?\n");
    	printf("\tDigite 1 para Crédito e 2 para Débito: "); 
        fflush (stdin); //Serve para limpeza de buffer. 
        scanf("%d", &esc);
         switch (esc)
         {
              case 1:
                   system("cls");
                   cabecalho();
                   printf("\t-------------------------------------\n");
                   printf("\t  >> O cartão será de Crédito <<\n");
                   printf("\t-------------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   fflush (stdin);
                   cabecalho();
                   printf("\t---------------------------------------------------\n");
                   printf("\t|*** INFORME O TIPO DE CARTÃO JUNTO À BANDEIRA ***|\n");
                   printf("\t|             Crédito (bandeira)                  |\n");
                   printf("\t---------------------------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   cabecalho();
                   printf("\n                                       ---------------------\n");
                   printf("Cartão e Bandeira (VISA OU MASTERCARD ): ");
                   fflush(stdin);
                   gets(band);
                   printf("                                       ---------------------\n\n");
                   
                   strcpy(primeiro->band,band);
                   fflush (stdin);
                   
              break;
              
              case 2:
                   system("cls");
                   cabecalho();
                   printf("\t--------------------------------\n");
                   printf("\t >>> O cartão será de Débito <<<\n");
                   printf("\t--------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   fflush(stdin);
                   cabecalho();
                   printf("\t---------------------------------------------------\n");
                   printf("\t|*** INFORME O TIPO DE CARTÃO JUNTO À BANDEIRA ***|\n");
                   printf("\t|             Débito (bandeira)                   |\n");
                   printf("\t---------------------------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   cabecalho();
                   printf("\n                       ---------------\n");
                   printf("Cartão e Bandeira (ELO): "); 
                   fflush(stdin); 
                   gets(band); 
                   printf("                       ---------------\n\n");
                   
                  strcpy(primeiro->band,band);// sinal -> é de recebe
	              fflush (stdin);
              break;
        
                    }   
                  getch();
                             
	    printf("\n\t--------- INFORMAÇÕES PARA O CARTÃO ----------\n\n");
        fflush (stdin); //Serve para limpaza de buffer.
	    printf("\nData de solicitação do cartão: ");
	    system("date/t"); //Pega a data atual do sistema.
	    printf("\n\tNome Completo: ",nome);
        fprintf(arq,"\n\tNome Completo: ");
	    fflush(stdin);
	    gets(nome);
        fgets(band,50,arq);
        fflush(stdin); //Serve para limpaza de buffer.
        printf("\n\tIdade: ");
        scanf("%d", &idade);
        fflush (stdin);
        if (idade >= 18){//CONDIÇÃO DA IDADE, ONDE SÓ SERÁ POSSIVEL SER FEITO O CADASTRO SE O CLIENTE FOR MAIOR DE IDADE.
                  printf("\n\t> Maior de idade! <\n");
                  strcpy(primeiro->nome,nome);
                  primeiro->idade=idade;	
	              fflush (stdin); //Serve para limpaza de buffer.
			  		
                  printf("\n\tSexo: ");  
                  gets(sexo);
                   while(strcmp(sexo, "F") && strcmp(sexo, "M") && strcmp(sexo, "f") && strcmp(sexo, "m") )
                   //A função strcmp compara duas strings e retorna 0 quando são iguais.
                   //CONDIÇÃO DO SEXO, ONDE SÓ ACEITARÁ SE FOR DIGITADO F OU M, TANTO MAIUSCULO QUANTO MINISCULO.
                   {
                               printf("\n\t****** Sexo inválido! *******");
                               printf("\n\n\tDigite F ou M: ");
                               gets(sexo);
                   }
                  strcpy(primeiro->sexo,sexo);
	              fflush (stdin);
	              
	              strcpy(cpf,"0");/*A função strlen calcula o tamanho da string excluindo o byte null ('\0') que indica
                   o fim da string.*/
	              while(strlen(cpf)!=11){
                   //A função strlen calcula o tamanho da string excluindo o byte null ('\0') que indica o fim da string.                      
				
                      printf("\n\tCPF: ");
                      fflush(stdin);
                      gets (cpf);
                      if (strlen(cpf)!=11)/*CONDIÇÃO DO CPF, ONDE SÓ SERÁ VÁLIDO SE FOR DIGITADO 11 DIGITOS, CASO FOR MAIOR OU
                  MENOS ESSE NÚMERO DE DIGITOS, UMA MENSAGEM APARECERÁ PARA QUE SE DIGITE NOVAMENTE O CPF CERTO.*/
                          printf("\n\t***** CPF inválido! ******\n\tDigite novamente. \n");
			                         }
                 strcpy(primeiro->cpf,cpf);
	             fflush (stdin);	
	             printf("\n\tRG: ");  
     	         scanf("%d", &rg);
     	         while(rg == 0){//CONDIÇÃO DO RG, ONDE SÓ SERÁ VÁLIDO UM RG DIFERENTE DE 0.
                       printf("\n\t******* Rg inválido! *******\n");
                       printf("\n\tDigite um Rg válido: ");
                       scanf("%d", &rg);
                          }
                 primeiro->rg=rg;
                 fflush(stdin);
       	         printf("\n\tTelefone de contato: ");
	             gets(primeiro->fone);
	             printf ("\n ------------------------------------------------------------\n");
                 printf ("   - Preencha os campos abaixo com informações do endereço -\n");
                 printf (" ------------------------------------------------------------\n");
	             fflush (stdin);
	             printf("\n\tEstado: ");
	             gets(primeiro->estado);
	             fflush (stdin);
	             printf("\n\tCidade: ");
	             gets(primeiro->cid);
	             fflush(stdin);
	             printf("\n\tRua: ");
	             gets(primeiro->rua);
     	         fflush (stdin);
	             printf("\n\tNúmero: ");
	             gets(primeiro->num);
	             fflush (stdin);
	             printf("\n\tComplemento: ");
	             gets(primeiro->comp);
	      
	             printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
	             printf("\n\tINFORME A RENDA MENSAL DO CLIENTE: ");
	             scanf("%d", &rend);/*ESSA CONDIÇÃO DECIDE QUANTO O CLIENTE TERÁ DE LIMITE NO SEU CARTÃO, ATRAVÉS DA INFOR-
                 MAÇÃO DE QUANTO ELE GANHA MENSALMENTE*/
	              if (rend < 300)
                      printf("\n\n>> O cartão terá limite de R$250,00 reais. <<\n");
                  if ((rend >= 300)&&(rend < 1000))
                      printf("\n\n\>> O cartão terá limete de R$850,00 reais. <<\n");
                  if ((rend >= 1000)&&(rend < 2000))
                      printf("\n\n>> O cartão terá limite de R$1.600,00 reais. <<\n");
                  if (rend >= 2000)
                      printf("\n\n>> O cartão terá limite de R$2.500,00 reais. <<\n");
                   
                  primeiro->rend,rend;
                 
                 
                 printf("\n\n------------------------------");
                 printf("\nCliente de código: %d", primeiro->cod=cod);
                 printf("\n------------------------------\n\n");
	              
	              printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
	              printf("O cliente deseja um cartão adicional?\n");
	              printf("Digite 1 para sim e 2 para não: ");
	              fflush(stdin);
	              scanf("%d", &esc);
	                if (esc == 1)
	                {
                           system("cls");
                           cabecalho();
                           printf("\n\t-------- INFORMAÇÕES PARA O CARTÃO ADICIONAL --------\n");
                           printf("\n\tNome completo: ");
                           fflush(stdin);
                           gets(primeiro->nomead);
                           printf("\n\tIdade: ");
                           fflush(stdin);
                           scanf("%d", &idadead);
                           primeiro->idadead=idadead;
                           printf("\n\tSexo: ");
                           fflush(stdin);
                           gets(sexoad);
                              while(strcmp(sexoad, "F") && strcmp(sexoad, "M") && strcmp(sexoad, "f") && strcmp(sexoad, "m") )
                              //A função strcmp compara duas strings e retorna 0 quando são iguais.
                              //CONDIÇÃO DO SEXO, ONDE SÓ ACEITARÁ SE FOR DIGITADO F OU M, TANTO MAIUSCULO QUANTO MINISCULO.
                              {
                                  printf("\n\t****** Sexo inválido! *******");
                                  printf("\n\n\tDigite F ou M: ");
                                  gets(sexoad);
                              }
                              strcpy(primeiro->sexoad,sexoad);
                              
                           strcpy(cpfad,"0");/*A função strlen calcula o tamanho da string excluindo o byte null ('\0') que 
                           indica o fim da string.*/
                             while(strlen(cpfad)!=11)
                             { /*A função strlen calcula o tamanho da string excluindo o byte null ('\0') que indica o fim 
                             da string. */                     
				                   printf("\n\tCPF: ");
                                   fflush(stdin);
                                   gets (cpfad);
                                   if (strlen(cpfad)!=11)/*CONDIÇÃO DO CPF, ONDE SÓ SERÁ VÁLIDO SE FOR DIGITADO 11 DIGITOS,
                                    CASO FOR MAIOR OU MENOS ESSE NÚMERO DE DIGITOS, UMA MENSAGEM APARECERÁ PARA QUE SE 
                                    DIGITE NOVAMENTE O CPF CERTO.*/
                                        printf("\n\t***** CPF inválido! ******\n\tDigite novamente. \n");
			                 }
                             strcpy(primeiro->cpfad,cpfad);
                             
                           printf("Telefone de contato: ");
                           fflush(stdin);
                           gets(primeiro->fonead);
                           
                     }
                     else {
                          system("cls");
                          cabecalho();
                          printf("\n\n\t      >> NENHUM CARTÃO ADICIONAL SOLICITADO <<\n\n\n\n\n");
                          }  
	       
                  system("pause");
                  }
            
        else{/*CASO O CLIENTE FOR MENOR DE IDADE, AQUI ELE MOSTRARÁ UMA MENSAGEM, INFORMANDO QUE O CLIENTE NÃO PODERÁ SOLI-
        CITAR UM CARTÃO POR SER MENOR DE IDADE E IRÁ ESPERAR O ENTER SER PRESSIONADO PARA VOLTAR AO MENU INICIAL*/
           system("cls");
           printf("\n\n---------------------------------------------------\n");
           printf("    Menor de idade, não pode solicitar um cartão! \n");
           printf("---------------------------------------------------\n\n\n\n");
           printf(">> PRESSIONE 'ENTER' PARA VOLTAR AO MENU INICIAL... <<");
           getch();
           return 0;
           }
             }
     else{
         system("cls");//SERVE PARA LIMPEZA DE TELA
         cabecalho();
         //APÓS NÃO CONSEGUIR REALIZAR O CADASTRO, POIS TEM RESTRIÇÃO DE CRÉDITO, UMA MENSAGEM SERÁ MOSTRADA.
         printf("\n--------------------------------------------------------------------------\n");
         printf("   Cliente com restrição de crédito, não é possivel realizar o cadastro! \n");
         printf("--------------------------------------------------------------------------\n\n\n\n");
         printf(">> PRESSIONE 'ENTER' PARA VOLTAR AO MENU INICIAL... <<");
         getch();
         return 0;
         }
     fclose(arq);
         primeiro->prox=NULL;  
         return (primeiro);
                       
}

/*---------------------------------------------------------------------------------------------------------------------*/

void  cadastrar_demais( struct nodo *inicio, int cod)/* void não manda nada de volta, só está recebeno um ponteiro do tipo
 struct nodo inicio*/
{
     int  idade, idadead, rend, esc, rg;
     char nome[30], nomead[30], cpfad[12], cpf[12], band[20], sexo[2], sexoad[2];
      struct nodo *aux, *novo;
      
      
    aux=inicio;
    while(aux->prox != NULL)
         aux = aux->prox;
    novo = (struct nodo *) malloc (sizeof(struct nodo)); // malloc é novo espaço de memória
 
     cabecalho();
     fflush (stdin); //Serve para limpaza de buffer
     printf("\tCliente possui restrição de crédito? \n"); //"printf" exibe mensagem em tela.
     printf("\tDigite 1 para sim e 2 para não: ");
     scanf("%d", &esc);
   
     if (esc == 2){
        system("cls");
        cabecalho();
        printf("\t--------- Informações para o Cartão ----------\n\n");
	    fflush (stdin); //Serve para limpaza de buffer.
	    printf("\tO cliente deseja cartão do tipo Crédito ou Débito?\n");
	    printf("\tDigite 1 para Crédito e 2 para Débito: ");
        fflush (stdin); //Serve para limpeza de buffer. 
        scanf("%d", &esc);
        switch (esc)
         {
              case 1:
                   system("cls");
                   cabecalho();
                   printf("\t-------------------------------------\n");
                   printf("\t  >> O cartão será de Crédito <<\n");
                   printf("\t-------------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   fflush (stdin);
                   cabecalho();
                   printf("\t---------------------------------------------------\n");
                   printf("\t|*** INFORME O TIPO DE CARTÃO JUNTO À BANDEIRA ***|\n");
                   printf("\t|             Crédito (bandeira)                  |\n");
                   printf("\t---------------------------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   cabecalho();
                   printf("\n\n                                       ---------------------\n");
                   printf("Cartão e Bandeira (VISA OU MASTERCARD ): ");
                   fflush(stdin);
                   gets(band);
                   printf("                                       ---------------------\n\n");
                   
                  strcpy(novo->band,band);
                   
              break;
              
              case 2:
                   system("cls");
                   cabecalho();
                   printf("\t--------------------------------\n");
                   printf("\t >>> O cartão será de Débito <<<\n");
                   printf("\t--------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   cabecalho();
                  system("cls");
                   fflush(stdin);
                   cabecalho();
                   printf("\t---------------------------------------------------\n");
                   printf("\t|*** INFORME O TIPO DE CARTÃO JUNTO À BANDEIRA ***|\n");
                   printf("\t|             Débito (bandeira)                   |\n");
                   printf("\t---------------------------------------------------\n\n\n");
                   printf(">> PRESSIONE 'ENTER' PARA CONTINUAR... <<");
                   getch();
                   system("cls");
                   cabecalho();
                   printf("\n\n                       ---------------\n");
                   printf("Cartão e Bandeira (ELO): "); 
                   fflush(stdin); 
                   gets(band); 
                   printf("                       ---------------\n\n");
                   
                   
                  strcpy(novo->band,band);
              break;
        
                    }   
                  getch();
                  
	    printf("\n\t--------- INFORMAÇÕES PARA O CARTÃO ----------\n\n");
	    printf("\nData de solicitação do cartão: ");
	    system("date/t"); //Pega a data atual do sistema.
   	
        fflush(stdin);
	    printf("\n\tNome Completo: ");
	    gets(nome);
	    fflush (stdin); //Serve para limpaza de buffer.
	    printf("\n\tIdade: ");
	    scanf("%d", &idade);
	    fflush (stdin);
        if (idade >= 18){//CONDIÇÃO DA IDADE, ONDE SÓ SERÁ POSSIVEL SER FEITO O CADASTRO SE O CLIENTE FOR MAIOR DE IDADE.
             printf("\n\t> Maior de idade! <\n");
             strcpy(novo->nome,nome);
             novo->idade=idade;
             fflush(stdin);	
             
                  printf("\n\tSexo: ");  
                  gets(sexo);
                   while(strcmp(sexo, "F") && strcmp(sexo, "M") && strcmp(sexo, "f") && strcmp(sexo, "m"))/*CONDIÇÃO DO 
                   SEXO, ONDE SÓ ACEITARÁ SE FOR DIGITADO F OU M, TANTO MAIUSCULO QUANTO MINISCULO*/
                   {
                                 printf("\n\t****** Sexo inválido! *******");
                                 printf("\n\n\tDigite F ou M: ");
                                 gets(sexo);
                                 }
                  strcpy(novo->sexo,sexo);
	              fflush (stdin);	

             fflush (stdin);	
	         strcpy(cpf,"0");
	         while(strlen(cpf)!=11){
				
                  printf("\n\tCPF: ");
                  fflush(stdin);
                  gets (cpf);
                  if (strlen(cpf)!=11)/*CONDIÇÃO DO CPF, ONDE SÓ SERÁ VÁLIDO SE FOR DIGITADO 11 DIGITOS, CASO FOR MAIOR OU
                  MENOS ESSE NÚMERO DE DIGITOS, UMA MENSAGEM APARECERÁ PARA QUE SE DIGITE NOVAMENTE O CPF CERTO.*/
			          printf("\n\t***** CPF inválido! ******\nDigite novamente. ");
			                      }
                  strcpy(novo->cpf,cpf);
             fflush (stdin);	
             printf("\n\tRG: ");  
             scanf("%d", &rg);
             while(rg == 0){//CONDIÇÃO DO RG, ONDE SÓ SERÁ VÁLIDO UM RG DIFERENTE DE 0.
                  printf("\n\t******* Rg inválido! *******\n");
                          printf("\n\tDigite um Rg válido: ");
                          scanf("%d", &rg);
                          }
             novo->rg=rg;
             fflush(stdin);
             printf("\n\tTelefone de contato: ");
             gets(novo->fone);
             printf ("\n -----------------------------------------------------------\n");
             printf ("   - Preencha os campos abaixo com informações do endereço -\n");
             printf ("  -----------------------------------------------------------\n");
             fflush (stdin);
             printf("\n\tEstado: ");
             gets(novo->estado);
             fflush(stdin);
             printf("\n\tCidade: ");
             gets(novo->cid);
             fflush(stdin);
             printf("\n\tRua: ");
             gets(novo->rua);
             fflush (stdin);
             printf("\n\tNúmero: ");
             gets(novo->num);
             fflush (stdin);
             printf("\n\tComplemento: ");
             gets(novo->comp);
           
             printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~");
             printf("\n\tINFORME A RENDA MENSAL DO CLIENTE: ");
             scanf("%d", &rend);/*ESSA CONDIÇÃO DECIDE QUANTO O CLIENTE TERÁ DE LIMITE NO SEU CARTÃO, ATRAVÉS DA INFORMA-
             ÇÃO DE QUANTO ELE GANHA MENSALMENTE*/
              if (rend < 300)
                  printf("\n\n>> O cartão terá limite de R$250,00 reais. <<\n");
              if ((rend >= 300)&&(rend < 1000))
                  printf("\n\n>> O cartão terá limete de R$850,00 reais. <<\n");
              if ((rend >= 1000)&&(rend < 2000))
                  printf("\n\n>> O cartão terá limite de R$1.600,00 reais. <<\n");
              if (rend >= 2000)
                  printf("\n\n>> O cartão terá limite de R$2.500,00 reais. <<\n");
                   
              novo->rend,rend; 
                
           
                printf("\n\n------------------------------");
                printf("\nCliente de código: %d", novo->cod=cod);
                printf("\n------------------------------\n\n");
                      
                      
                printf("\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
                printf("O cliente deseja um cartão adicional?\n");
                printf("Digite 1 para sim e 2 para não: ");
                fflush(stdin);
                scanf("%d", &esc);
	                if (esc == 1)
	                {
                          system("cls");
                          cabecalho();
                          printf("\t-------- INFORMAÇÕES PARA O CARTÃO ADICIONAL --------\n");
                          printf("\n\tNome completo: ");
                          fflush(stdin);
                          gets(novo->nomead);
                          printf("\n\tIdade: ");
                          fflush(stdin);
                          scanf("%d", &idadead);
                          novo->idadead=idadead;
                          printf("\n\tSexo: ");
                          fflush(stdin);
                          gets(sexoad);
                            while(strcmp(sexoad, "F") && strcmp(sexoad, "M") && strcmp(sexoad, "f") && strcmp(sexoad, "m"))
                              //A função strcmp compara duas strings e retorna 0 quando são iguais.
                              //CONDIÇÃO DO SEXO, ONDE SÓ ACEITARÁ SE FOR DIGITADO F OU M, TANTO MAIUSCULO QUANTO MINISCULO.
                            {
                                printf("\n\t****** Sexo inválido! *******");
                                printf("\n\n\tDigite F ou M: ");
                                gets(sexoad);
                            }
                            strcpy(novo->sexoad,sexoad);
                              
                          strcpy(cpfad,"0");/*A função strlen calcula o tamanho da string excluindo o byte null ('\0') que 
                           indica o fim da string.*/
                             while(strlen(cpfad)!=11)
                             { /*A função strlen calcula o tamanho da string excluindo o byte null ('\0') que indica o fim 
                             da string. */                     
				                   printf("\n\tCPF: ");
                                   fflush(stdin);
                                   gets (cpfad);
                                   if (strlen(cpfad)!=11)/*CONDIÇÃO DO CPF, ONDE SÓ SERÁ VÁLIDO SE FOR DIGITADO 11 DIGITOS,
                                    CASO FOR MAIOR OU MENOS ESSE NÚMERO DE DIGITOS, UMA MENSAGEM APARECERÁ PARA QUE SE 
                                    DIGITE NOVAMENTE O CPF CERTO.*/
                                        printf("\n\t***** CPF inválido! ******\n\tDigite novamente. \n");
			                 }
                             strcpy(novo->cpfad,cpfad);
                             
                         printf("Telefone de contato: ");
                         fflush(stdin);
                         gets(novo->fonead);
                             
                 }
                 else {
                     system("cls");
                     cabecalho();
                     printf("\n\n\t      >> NENHUM CARTÃO ADICIONAL SOLICITADO <<\n\n\n\n\n");
                      }     
    
                      system("pause");
              
              novo->prox=NULL;
              aux->prox = novo;
    
                     }

        else{/*AQUI ELE MOSTRARÁ UMA MENSAGEM, INFORMANDO QUE O CLIENTE NÃO PODERÁ SOLICITAR UM CARTÃO POR SER MENOR DE
        IDADE E IRÁ ESPERAR O ENTER SER PRESSIONADO PARA VOLTAR AO MENU INICIAL*/
           system("cls");
           printf("\n---------------------------------------------------\n");
           printf("    Menor de idade, não pode solicitar um cartão! \n");
           printf("---------------------------------------------------\n\n\n\n");
           printf(">> PRESSIONE 'ENTER' PARA VOLTAR AO MENU INICIAL... <<");
           getch();
           }
           }
     else{
         system("cls");
         printf("\n--------------------------------------------------------------------------\n");
         printf("   Cliente com restrição de crédito, não é possivel realizar o cadastro! \n");
         printf("--------------------------------------------------------------------------\n\n\n\n");
         printf(">> PRESSIONE 'ENTER' PARA VOLTAR AO MENU INICIAL... <<");
         getch();
         
         } 
                    
}

/*----------------------------------------------------------------------------------------------------------------------*/

void listar (struct nodo *primeiro)
{
     struct nodo *aux;

     aux=primeiro;
     
     while(aux!= NULL)
     {
        cabecalho();
        printf("------------------------------------------------------------------\n");
        printf("\n\t--------- INFORMAÇÕES PARA O CARTÃO ----------\n\n");
        printf("Código do cliente: %d\n\n", aux->cod);
        printf("Tipo e Bandeira: %s\n\n", aux->band);
        printf("Nome Completo: %s\n\n", aux->nome);
        printf("Idade: %d\n\n", aux->idade);
        printf("Sexo: %s\n\n", aux->sexo);
        printf("Cpf:  %s\n\n", aux->cpf);
        printf("RG:  %d\n\n", aux->rg);
        printf("Telefone de contato:  %s\n\n", aux->fone);
        printf ("\n ------------------------------\n");
        printf ("   - Informações do endereço -\n");
        printf (" ------------------------------\n\n");
        printf("Estado:  %s\n\n", aux->estado);
        printf("Cidade:  %s\n\n", aux->cid);
        printf("Rua:  %s\n\n", aux->rua);
        printf("Número:  %s \n\n", aux->num);
        printf("Complemento:  %s\n\n", aux->comp);
       // printf("Renda mensal:  %d\n\n", aux->rend);
        printf("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n");
        printf("--------- INFORMAÇÕES PARA O CARTÃO ADICIONAL ----------\n\n");
        printf("Nome Completo: %s\n\n", aux->nomead);
        printf("Idade: %d\n\n", aux->idadead);
        printf("Sexo: %s\n\n", aux->sexoad);
        printf("Cpf: %s\n\n", aux->cpfad);
        printf("Telefone de contato: %s\n\n", aux->fonead);
        printf("---------------------------------------------------------------------\n");
        aux = aux->prox;
        //dê um enter para aparecer o próximo

        printf("\n>> Tecle enter para listar o próximo cadastro! <<\n\n");
        
        getch();
     }
      aux->prox == NULL;
      system("cls");
      printf("\n\n\n\t>>> NÃO EXISTE MAIS CADASTRO <<< \n");
}


/*---------------------------------------------------------------------------------------------------------------------*/

struct nodo * editar(){ //OPÇÃO 2: EDITAR
	struct nodo *editar;
	editar = (struct nodo *) malloc (sizeof(struct nodo)); // Reservando um espaço em memória para receber editar
	editar->prox=NULL;   
    return (editar);  
}

/*--------------------------------------------------------------------------------------------------------------------*/

struct nodo * pesq(){ //OPÇÃO 3: PESQUISAR
	struct nodo *pesq;
	pesq = (struct nodo *) malloc (sizeof(struct nodo)); // Reservando um espaço em memória para receber pesq
	pesq->prox=NULL;   
    return (pesq);  
}

/*------------------------------------------------------------------------------------------------------------------*/

struct nodo * excluir(struct nodo *primeiro)
{
     struct nodo *aux,*proximo, *retorno, *anterior=NULL;
     char nm[51];
     
     aux = primeiro;
     cabecalho();
     printf("Informe o nome a ser excluido: ");
     gets(nm);
     
     while((strcmp(aux->nome,nm)!=0)&&(aux->prox!=NULL))
     {
          anterior=aux;
          aux = aux->prox;
     }
     
     printf("%s\n",aux->nome);
     
          
     if (strcmp(aux->nome,nm)==0)
     {
        if (aux == primeiro) //primeiro elemento
           if (aux->prox == NULL){ //só tem um
             
              free(aux);
              retorno = NULL;
                                 }
           else{ //lista com elementos eliminando o 1º
           
               retorno = aux->prox;
               free(aux);
               }
        else
            if (aux->prox == NULL){ // é o ultimo
               anterior->prox = NULL;
               free(aux);
               retorno = primeiro;
                                  }
            else{   //demais
            
                   anterior->prox = aux->prox;
                   free(aux);
                   retorno = primeiro;
                }
            printf("Cliente excluido.");
            return(retorno);
     }
     else
     {
        printf("Cliente não cadastrado.");
        return(primeiro);
     }
     free(primeiro);
}
  
 /*------------------------------------------------------------------------------------------------------------------*/ 
      
int main (void){ //FUNÇÃO PRINCIPAL
	
	setlocale(LC_ALL, "Portuguese"); //Serve para informar o idioma/região que se quer que o programa leia. Bastante útil no caso da língua portuguesa, que se utiliza acentos. 
	system ("color 0b"); /*A função system("color 0f") muda a cor do fundo da tela e das fontes. */
	  int cod=0, op=0;
	  
	  
      while(op!=5)
      {
         op = menu(); // menu é uma função.
         
       if (op==1){
            if (inicio == NULL) 
               inicio = cadastrar_primeiro(cod);
            else
               cadastrar_demais(inicio, cod);
               cod++;

                }  
                  
       	if (op==2){
             if (inicio==NULL)
               printf("Nao foram feitos cadastros.");
            else
                listar(inicio);
            getch();
                  }
                   
	  	if (op==3){
            if (inicio == NULL) 
            inicio = editar();
                   }       
  
        if (op==4){
           if (inicio == NULL) 
            inicio = pesq();
                  }
                  
        if (op==5){
            if (inicio==NULL)
               printf("Não foram feitos cadastros.");
            else
                inicio=excluir(inicio);
            getch();
                  }
                  
        if (op==6){
            system("cls");
            cabecalho();
            printf("\t----------------------------------\n");
    		printf("\t    VOCÊ IRÁ SAIR DO PROGRAMA  \n");
    		printf("\t----------------------------------\n\n\n\n");
    		printf("\n\n>>> PRESSIONE ENTER PARA SAIR! <<<\n\n");
    		getch();
			return 0;
                   }
      }
      getch();
}   

void cabecalho(){
     system("cls");
     printf("\n                    CADASTRO DE SOLICITAÇÃO DE CARTÃO \n");
     printf(" ==============================================================================\n\n\n");
}
